***Conectar no cluster

aws eks update-kubeconfig --region us-east-2 --name k8s-demo

***Setup IAM role for service accounts

eksctl utils associate-iam-oidc-provider \
    --region us-east-2 \
    --cluster k8s-demo \
    --approve

***Download IAM policy for the AWS Load Balancer Controller
curl -o iam-policy.json https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.3.1/docs/install/iam_policy.json

***Create an IAM policy called AWSLoadBalancerControllerIAMPolicy

aws iam create-policy \
    --policy-name AWSLoadBalancerControllerIAMPolicy \
    --policy-document file://iam-policy.json

    Retorna Arn-anotar 977757868540:policy/AWSLoadBalancerControllerIAMPolicy

***Create a IAM role and ServiceAccount for the AWS Load Balancer controller, use the ARN from the step above

eksctl create iamserviceaccount \
--cluster=k8s-demo \
--namespace=kube-system \
--name=aws-load-balancer-controller \
--attach-policy-arn=arn:aws:iam::977757868540:policy/AWSLoadBalancerControllerIAMPolicy \
--override-existing-serviceaccounts \
--region us-east-2 \
--approve

***Add Controller to Cluster "Via Helm"

### Detailed Instructions
Follow the instructions in [aws-load-balancer-controller](https://github.com/aws/eks-charts/tree/master/stable/aws-load-balancer-controller) helm chart.

### Summary

1. Add the EKS chart repo to helm
```
helm repo add eks https://aws.github.io/eks-charts
```
1. Install the TargetGroupBinding CRDs if upgrading the chart via `helm upgrade`.
```
kubectl apply -k "github.com/aws/eks-charts/stable/aws-load-balancer-controller//crds?ref=master"
```

    !!!tip
        The `helm install` command automatically applies the CRDs, but `helm upgrade` doesn't.

    !!!tip
        Only run one of the two following `helm install` commands depending on whether or not your cluster uses IAM roles for service accounts.

1. Install the helm chart if using IAM roles for service accounts. **NOTE** you need to specify both of the chart values `serviceAccount.create=false` and `serviceAccount.name=aws-load-balancer-controller`
```
helm install aws-load-balancer-controller eks/aws-load-balancer-controller -n kube-system --set clusterName=k8s-demo --set serviceAccount.create=false --set serviceAccount.name=aws-load-balancer-controller
```
1. Install the helm chart if **not** using IAM roles for service accounts
```
helm install aws-load-balancer-controller eks/aws-load-balancer-controller -n kube-system --set clusterName=k8s-demo
```
helm delete aws-load-balancer-controller eks/aws-load-balancer-controller -n kube-system --set clusterName=k8s-demo